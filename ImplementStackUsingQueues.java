import java.util.LinkedList;
import java.util.Queue;

class MyStack {
    private Queue<Integer> queue1;
    private Queue<Integer> queue2;
    private int topElement;

    /** Initialize your data structure here. */
    public MyStack() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    /** Push element x onto stack. */
    public void push(int x) {
        // Add the new element to the empty queue
        if (queue1.isEmpty()) {
            queue1.offer(x);
        } else {
            // Transfer all elements from queue1 to queue2
            while (!queue1.isEmpty()) {
                queue2.offer(queue1.poll());
            }
            // Add the new element to queue1
            queue1.offer(x);
            // Transfer all elements back from queue2 to queue1
            while (!queue2.isEmpty()) {
                queue1.offer(queue2.poll());
            }
        }
        // Update topElement to the newly pushed element
        topElement = x;
    }

    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        int popped = queue1.poll();
        if (!queue1.isEmpty()) {
            // Update topElement to the new top element after popping
            topElement = queue1.peek();
        }
        return popped;
    }

    /** Get the top element. */
    public int top() {
        return topElement;
    }

    /** Returns whether the stack is empty. */
    public boolean empty() {
        return queue1.isEmpty();
    }
}
