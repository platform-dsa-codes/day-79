//{ Driver Code Starts
import java.util.*;


class StackUsingQueues
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t>0)
		{
			Queues g = new Queues();
			
			int q = sc.nextInt();
			while(q>0)
			{
				int QueryType = sc.nextInt();
				if(QueryType == 1)
				{
					int a = sc.nextInt();
					g.push(a);
				}
				else if(QueryType == 2)
				System.out.print(g.pop()+" ");
			q--;
			}	
			System.out.println();
				
			
			
		t--;
		}
	}
}


// } Driver Code Ends


class Queues {
    Queue<Integer> q1 = new LinkedList<Integer>();
    Queue<Integer> q2 = new LinkedList<Integer>();
    
    //Function to push an element into stack using two queues.
    void push(int a) {
        // Push the new element into the non-empty queue
        if (!q1.isEmpty()) {
            q1.offer(a);
        } else {
            q2.offer(a);
        }
    }
    
    //Function to pop an element from stack using two queues. 
    int pop() {
        if (q1.isEmpty() && q2.isEmpty())
            return -1; // Stack is empty

        // Transfer all elements from the non-empty queue to the other queue
        if (!q1.isEmpty()) {
            while (q1.size() > 1) {
                q2.offer(q1.poll());
            }
            return q1.poll();
        } else {
            while (q2.size() > 1) {
                q1.offer(q2.poll());
            }
            return q2.poll();
        }
    }
}
